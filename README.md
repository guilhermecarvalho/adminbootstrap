== Bem vindo ao AdminBootStrap

Esse projeto tem por finalidade a criação de uma alternativa aos templates do Admin no Django, utilizando o BootStrap como engine.

== Início

1. As configurações necessárias já estão feitas no settings

2. Na app base tem o menu_topo.py que é resposável por criar o menu drop-down da barra superior


== Contribuição

Lembre-se de sempre enviar todas as melhorias e novas funcionalidades que vc desenvolver, assim ampliaremos as funcionalidades dessa interface

== Licença

Open Source
