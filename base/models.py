# -*- coding:utf-8 -*-

from django.db import models


class Parametrizacao(models.Model):
    u"""Classe para parametrizacao do sistema"""
    email_padrao = models.EmailField(u'Email padrão', max_length=200)
    url = models.URLField(u'URL do site da empresa')
